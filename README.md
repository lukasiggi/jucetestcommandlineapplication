# JUCETestCommandLineApplication

A small command-line application using JUCE, intended for testing purposes.
In order to compile this project, JUCE needs to be cloned to your home directory.

## Dependencies

- Min. JUCE 6.0.0
