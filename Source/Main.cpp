/*
  ==============================================================================

    This file contains the basic startup code for a JUCE application.

  ==============================================================================
*/

#include <JuceHeader.h>

using namespace juce;

//==============================================================================
int main (int argc, char* argv[])
{
    ConsoleApplication app;

    app.addHelpCommand ("--help|-h", "Usage:", true);
    app.addVersionCommand ("--version|-v", "MyApp version 1.2.3");

    // Your code goes here!
    juce::ignoreUnused (argc, argv);

    return app.findAndRunCommand (argc, argv);
}
